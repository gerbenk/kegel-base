package it.kegel.base.todo

import java.util.*


data class ErrorDetails(val timestamp: Date, val message: String, val details: String)