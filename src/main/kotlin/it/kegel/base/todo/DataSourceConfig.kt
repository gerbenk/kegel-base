package it.kegel.base.todo

import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.amazonaws.services.secretsmanager.model.*
import com.beust.klaxon.Klaxon
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.File
import java.util.*
import javax.sql.DataSource
import kotlin.system.exitProcess


@Configuration
class DataSourceConfig() {

    @Value("\${spring.configfile}")
    lateinit var configFilename: String
    @Value("\${spring.aws.secretsmanager.secretName}")
    lateinit var secretName: String
    @Value("\${spring.aws.secretsmanager.region}")
    lateinit var region: String

    @Bean
    fun dataSource(): DataSource {
        val credentials = getCredentials()
        val url = credentials.getUrl()

        val dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver")
        dataSourceBuilder.url(url)
        dataSourceBuilder.username(credentials.username)
        dataSourceBuilder.password(credentials.password)
        return dataSourceBuilder.build()
    }

    private fun getCredentials(): DatabaseCredentials {
        try {
            val secret = getSecretFromAwsSecretManager()
            val credentials = Klaxon().parse<DatabaseCredentials>(secret)
            println("Database username: ${credentials!!.username}")
            return credentials
        } catch(e: Exception) {
            println("Credentials could not be fetched from AWS Secret Manager. Getting local values. Reason : ${e.message}")
            return getCredentialsFromConfigFile()
        }
    }

    private fun getSecretFromAwsSecretManager(): String {
        val client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(region)
                .build()
        val getSecretValueRequest = GetSecretValueRequest()
                .withSecretId(secretName)

        val getSecretValueResult = try {
            client.getSecretValue(getSecretValueRequest)
        } catch (e: DecryptionFailureException) {
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e
        } catch (e: InternalServiceErrorException) {
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e
        } catch (e: InvalidParameterException) {
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e
        } catch (e: InvalidRequestException) {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e
        } catch (e: ResourceNotFound) {
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e
        }

        if (getSecretValueResult.secretString != null) {
            return getSecretValueResult.secretString
        } else {
            return String(Base64.getDecoder().decode(getSecretValueResult.secretBinary).array())
        }
    }

    private fun getCredentialsFromConfigFile(): DatabaseCredentials {
        val file = File(configFilename)
        if (file.exists()) {
            val content = file.readText()
            val credentials = Klaxon().parse<DatabaseCredentials>(content)
            println("Username: ${credentials!!.username}")
            return credentials
        } else {
            System.err.println("No database credentials specified in Secret Manager or $configFilename")
            exitProcess(1)
        }
    }

    private data class DatabaseCredentials(val username: String, val password: String, val host: String, val port: Int) {
        val database = "todo"

        fun getUrl() = "jdbc:mysql://${host}:${port}/$database?useSSL=false"
    }
}