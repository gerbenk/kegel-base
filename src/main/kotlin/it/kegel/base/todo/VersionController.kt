package it.kegel.base.todo

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import it.kegel.base.Manifest
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/version")
@Api(value="Todolist", description="Operations for showing version")
class VersionController() {

    @GetMapping
    @ApiOperation(value = "View version")
    fun tasks() = ResponseEntity.ok(readVersion())

    private fun readVersion(): String {
        val optionalVersion = Manifest.getValue("Implementation-Version")
        return if (optionalVersion.isPresent) optionalVersion.get() else "0.0"
    }
}