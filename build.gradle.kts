import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val title = "TodoService"
group = "it.kegel"
version = "0.2-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

plugins {
    id("org.springframework.boot") version "2.1.13.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.3.40"
    kotlin("plugin.spring") version "1.3.40"
    kotlin("plugin.jpa") version "1.3.40"
    application
    jacoco
}

repositories {
    mavenCentral()
    jcenter()
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform {}

    testLogging.showStandardStreams = true
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect:1.3.40"))
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.2")
    implementation("com.beust:klaxon:5.0.12")
    implementation("com.amazonaws:aws-java-sdk-secretsmanager:1.11.549")
    implementation("io.springfox:springfox-swagger2:2.8.0")
    implementation("io.springfox:springfox-swagger-ui:2.8.0")
//    implementation("io.springfox:springfox-bean-validators:2.8.0")

    runtimeOnly("mysql:mysql-connector-java:5.1.49")

    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.1")
    testImplementation("io.mockk:mockk:1.9.3")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude("org.junit.vintage", "junit-vintage-engine")
    }
}

tasks {
    getByName<JacocoReport>("jacocoTestReport") {
        afterEvaluate {
            getClassDirectories().setFrom(files(classDirectories.files.map {
                fileTree(it) { exclude("**/ui/**") }
            }))
        }
    }
    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "1.8"
        }
    }
    bootJar {
        manifest {
            attributes(
                    "Implementation-Title" to title,
                    "Implementation-Version" to archiveVersion
            )
        }
    }
}

jacoco {
    toolVersion = "0.8.3"
}

application {
    mainClassName = "it.kegel.base.todo.TodoApplicationKt"
}

defaultTasks("clean", "test", "jacocoTestReport")

