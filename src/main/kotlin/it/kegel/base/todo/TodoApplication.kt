package it.kegel.base.todo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import java.util.concurrent.Executor

@SpringBootApplication
@EnableScheduling
class TodoApplication {

	@Bean
	fun taskExecutor(): Executor? {
		val executor = ThreadPoolTaskExecutor()
		executor.corePoolSize = 2
		executor.maxPoolSize = 2
		executor.setQueueCapacity(500)
		executor.setThreadNamePrefix("Async-")
		executor.initialize()
		return executor
	}
}

fun main(args: Array<String>) {
	runApplication<TodoApplication>(*args)
}
