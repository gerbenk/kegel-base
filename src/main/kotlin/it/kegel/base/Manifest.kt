package it.kegel.base

import java.io.*
import java.util.*

object Manifest {
    fun getValue(key: String?): Optional<String> {
        try {
            val resources = Manifest::class.java.classLoader
                    .getResources("META-INF/MANIFEST.MF")
            while (resources.hasMoreElements()) {
                val resource = resources.nextElement()
                if (resource.path.contains("BOOT-INF"))
                    continue;
                val manifest = java.util.jar.Manifest(resource.openStream())
                val attrs = manifest.mainAttributes
                val value = attrs.getValue(key)
                if (value != null) {
                    return Optional.of(value)
                }
            }
        } catch (ex: IOException) {
        }
        return Optional.empty()
    }
}