package it.kegel.base.todo

import java.util.*

class UnknownTask(id: UUID) : Exception("Unknown id $id") {
}