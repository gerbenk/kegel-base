package it.kegel.base.todo

import io.swagger.annotations.*
import it.kegel.base.todo.Task
import it.kegel.base.todo.TaskService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/tasks")
@Api(value="Todolist", description="Operations for managing tasks")
class TaskController(val service: TaskService) {

    @GetMapping
    @ApiOperation(value = "View a list of tasks")
    @ApiResponses(
            ApiResponse(code = 200, message = "The task list was successfully retrieved")
    )
    fun tasks(): ResponseEntity<Iterable<Task>> {
        return ResponseEntity.ok(service.getAll().get())
    }

    @PostMapping
    @ApiOperation(value = "Add a task")
    @ApiResponses(
            ApiResponse(code = 200, message = "The task was successfully added")
    )

    fun create(@ApiParam(value = "Task definition", required = true) @RequestBody task: Task): ResponseEntity<Task> {
        service.save(task)

        return ResponseEntity.ok(task)
    }

    @PostMapping("/{id}/check")
    @ApiOperation(value = "Check a task")
    @ApiResponses(
            ApiResponse(code = 200, message = "The task was successfully checked"),
            ApiResponse(code = 404, message = "The task does not exist")
    )
    fun check(@ApiParam(value = "Task ID", required = true) @PathVariable id: UUID): ResponseEntity<Task> {
        val task = service.get(id)
        service.check(id)

        return ResponseEntity.ok(task)
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete a task")
    @ApiResponses(
        ApiResponse(code = 204, message = "The task does not exist anymore")
    )
    fun delete(@ApiParam(value = "Task ID", required = true) @PathVariable id: UUID) : ResponseEntity<Void>  {
        service.delete(id)
        return ResponseEntity.noContent().build();
    }
}