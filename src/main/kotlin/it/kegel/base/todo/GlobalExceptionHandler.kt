package it.kegel.base.todo

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.context.request.WebRequest
import java.util.*


@ControllerAdvice
class GlobalExceptionHandler {
    @ControllerAdvice
    class GlobalExceptionHandler {
        @ExceptionHandler(ResourceNotFound::class)
        @ResponseStatus(value=HttpStatus.NOT_FOUND)
        fun resourceNotFoundException(ex: ResourceNotFound, request: WebRequest): ResponseEntity<*> {
            val errorDetails = ErrorDetails(Date(), ex.message ?: "", request.getDescription(false))
            return ResponseEntity<Any>(errorDetails, HttpStatus.NOT_FOUND)
        }

        @ExceptionHandler(Exception::class)
        fun globalExcepetionHandler(ex: Exception, request: WebRequest): ResponseEntity<*> {
            val errorDetails = ErrorDetails(Date(), ex.message ?: "", request.getDescription(false))
            return ResponseEntity<Any>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}