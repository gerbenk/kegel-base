package it.kegel.base.todo

import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import java.util.concurrent.CompletableFuture

@Service
@Transactional
class TaskService(val repository: TaskRepository) {
    @Async
    fun getAll(): CompletableFuture<Iterable<Task>> {
        return CompletableFuture.completedFuture(repository.findAll())
    }

    fun get(id: UUID): Task {
        val optionalTask = repository.findById(id)
        if (optionalTask.isPresent) {
            return optionalTask.get()
        } else {
            throw UnknownTask(id)
        }
    }

    fun save(task: Task)  {
        repository.save(task)
    }

    fun delete(id: UUID) {
        val found = repository.existsById(id)
        if (found) {
            repository.deleteById(id)
        } else {
            throw UnknownTask(id)
        }
    }

    fun check(id: UUID) {
        val task = repository.findById(id)
        if (task.isPresent) {
            task.get().check()
        } else {
            throw UnknownTask(id)
        }
    }
}