package it.kegel.base.todo

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

@Entity
@ApiModel(description = "All details about the Task.")
data class Task(
        @ApiModelProperty(notes = "The name of the Task") val name: String,
        @ApiModelProperty(notes = "The ID of the Task") @Id val id: UUID = UUID.randomUUID()) {

    @ApiModelProperty(notes = "Whether this task is checked")
    private var checked = false

    fun isChecked() = checked

    fun check(): Unit {
        checked = true
    }

    fun uncheck(): Unit {
        checked = false
    }

}
